﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class carController : MonoBehaviour
{
    public List<AxleInfo> axleInfos; 
    public float maxMotorTorque; 
    public float maxSteeringAngle;

    float v; 

    public KeyCode brake;
    public KeyCode options;

    public GameObject FR;
    public GameObject FL;
    public GameObject BR;
    public GameObject BL;

    public GameObject pause;

    public GameObject humo;

    WheelFrictionCurve fr;
    WheelFrictionCurve fl;

    internal void activar(bool v)
    {
        if(v == false)
        {
            Destroy(this);
        }
    }

    public bool lap = false;

    private void Start()
    {
        pause = GameObject.Find("Pause");
        pause.gameObject.SetActive(false);

        v = this.gameObject.GetComponent<Rigidbody>().velocity.x * this.gameObject.transform.forward.x + this.gameObject.GetComponent<Rigidbody>().velocity.z * this.gameObject.transform.forward.z;

        fr = BR.GetComponent<WheelCollider>().sidewaysFriction;
        fl = BL.GetComponent<WheelCollider>().sidewaysFriction;

        print(BR.GetComponent<WheelCollider>().sidewaysFriction.stiffness);
    }

    public void FixedUpdate()
    {
        v = this.gameObject.GetComponent<Rigidbody>().velocity.x * this.gameObject.transform.forward.x + this.gameObject.GetComponent<Rigidbody>().velocity.z * this.gameObject.transform.forward.z;

        if (v <= 10)
        {
            maxMotorTorque = 2000;
        }
        else
        {
            maxMotorTorque = 1300;
        }

        float motor = maxMotorTorque *Input.GetAxis("Vertical");

        if (Input.GetAxis("Vertical") > 0)
        {
            motor = maxMotorTorque * Input.GetAxis("Vertical");
        }else if (Input.GetAxis("Vertical") < 0)
        {
            motor = maxMotorTorque * Input.GetAxis("Vertical") * 1.5f;
        }
        float steering = maxSteeringAngle * Input.GetAxis("Horizontal");

        foreach (AxleInfo axleInfo in axleInfos)
        {
            if (axleInfo.steering)
            {
                axleInfo.leftWheel.steerAngle = steering;
                axleInfo.rightWheel.steerAngle = steering;
            }
            if (axleInfo.motor)
            {
                axleInfo.leftWheel.motorTorque = motor;
                axleInfo.rightWheel.motorTorque = motor;
            }
        }

        if (Input.GetKey(brake))
        {
            fr.stiffness = 1;
            fl.stiffness = 1;
            BR.GetComponent<WheelCollider>().sidewaysFriction = fr;
            BL.GetComponent<WheelCollider>().sidewaysFriction = fl;

            humo.SetActive(true);
        }
        else
        {
            fr.stiffness = 5;
            fl.stiffness = 5;
            BR.GetComponent<WheelCollider>().sidewaysFriction = fr;
            BL.GetComponent<WheelCollider>().sidewaysFriction = fl;

            humo.SetActive(false);
        }

        if (Input.GetKeyDown(options)){
            if (pause.gameObject.active == true)
            {
                pause.gameObject.SetActive(false);
            }
            else
            {
                pause.gameObject.SetActive(true);
            }
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("out"))
        {
            SceneManager.LoadScene("Circuito");
        }
    }

}
    [System.Serializable]
    public class AxleInfo
    {
        public WheelCollider leftWheel;
        public WheelCollider rightWheel;
        public bool motor;
        public bool steering;
    }




