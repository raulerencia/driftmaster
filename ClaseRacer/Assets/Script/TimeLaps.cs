﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeLaps : MonoBehaviour
{
    float bestLap = 9999999999999999999;
    float lap;
    public GameObject player;
    public GameObject improveTime;
    public GameObject TextBestLap;
    private float StartTime;
    private float TimerControl;
    public Text timeLapCanvas;


    void Start()
    {
        StartTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (lap > 0)
        {
            lap -= 1 * Time.deltaTime;
            timeLapCanvas.text = lap.ToString();
        }
        else
        {
            timeLapCanvas.text = "Not improved";
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("player"))
        {
            
            if(player.gameObject.GetComponent<carController>().lap)
            {
                improveTime.gameObject.SetActive(true);
                TimerControl = Time.time - StartTime;
                if (TimerControl < bestLap)
                {
                    bestLap = TimerControl;
                }
                string mins = ((int)bestLap / 60).ToString("00");
                string segs = (bestLap % 60).ToString("00");
                string milisegs = ((bestLap * 100) % 100).ToString("00");

                string TimerString = string.Format("{00}:{01}:{02}", mins, segs, milisegs);

                TextBestLap.gameObject.GetComponent<TextMesh>().text = TimerString;
            }

            player.gameObject.GetComponent<carController>().lap = true;
            lap = bestLap;
           
        }
        StartTime = Time.time;
    }
}
