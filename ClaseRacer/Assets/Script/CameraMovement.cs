﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    GameObject p;
    //Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        
        //offset = new Vector3(p.transform.position.x, p.transform.position.y + 1, p.transform.position.z - 3);
    }

    // Update is called once per frame
    void Update()
    {
        //this.transform.position = Vector3.Lerp(this.transform.position, p.transform.position + offset, p.GetComponent<Movement>().speed * Time.deltaTime);
        p = GameObject.Find("Player");
        this.transform.position = new Vector3
        (
            //volem la posició no exactament en el jugador, sino que la volem una mica enrere i damunt seu. Si el jugador gira la camara s'hauria de moure respecte al seu nou darrere, orbitant
            Mathf.Lerp(this.transform.position.x, (p.transform.position.x - (p.transform.forward.x * 8)), Time.deltaTime * 10),
            Mathf.Lerp(this.transform.position.y, (p.transform.position.y - (p.transform.forward.y * 10)) + 3, Time.deltaTime * 10),
            Mathf.Lerp(this.transform.position.z, (p.transform.position.z - (p.transform.forward.z * 8)), Time.deltaTime * 10)
        );
        this.transform.LookAt(p.transform);
        
    }
}
