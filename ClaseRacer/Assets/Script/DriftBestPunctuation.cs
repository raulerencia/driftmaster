﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "BestPunctutaion", menuName = "BestPunctutaion Data", order = 25)]
public class DriftBestPunctuation : ScriptableObject
{
    [SerializeField]
    private float bestPunctuation;
    public float BestPunctuation { get; set; }
}
