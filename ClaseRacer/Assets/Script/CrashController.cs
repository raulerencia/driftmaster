﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrashController : MonoBehaviour
{

    public GameObject player;
    public GameObject fire;

    // Start is called before the first frame update
    void Start()
    {
        fire.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        player.gameObject.GetComponent<carController>().activar(false);
        fire.gameObject.SetActive(true);
    }

}
