﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LucesFreno : MonoBehaviour
{

    GameObject frenos;

    public KeyCode brake;

    // Start is called before the first frame update
    void Start()
    {
        frenos = GameObject.Find("Frenos");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("s") || Input.GetKey(brake))
        {
            frenos.gameObject.SetActive(true);
        }
        else
        {
            frenos.gameObject.SetActive(false);
        }
    }
}
