﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Velocimetro : MonoBehaviour
{

    public GameObject player;
    public Text t;
    float v;
    //public GameObject barra;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        v = player.gameObject.GetComponent<Rigidbody>().velocity.x * player.gameObject.transform.forward.x + player.gameObject.GetComponent<Rigidbody>().velocity.z * player.gameObject.transform.forward.z;

        v = Mathf.Floor(v);
        v = v * 4;

        if (v >= 0)
        {
            t.text = v.ToString();
        }
        else
        {
            t.text = 0.ToString();
        }

        //barra.gameObject.GetComponent<Image>().fillAmount = v / 100;
        
    }
}
