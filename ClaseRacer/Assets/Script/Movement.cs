﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float speed = 250f;
    public float rotationSpeed = 100.0f;
    public KeyCode derrape; 

    // Start is called before the first frame update
    void Start()
    {
        
    }

    void FixedUpdate()
    {
        Move();
    }

    private void Move()
    {
           float translation = Input.GetAxis("Vertical") * speed;
           float rotation = Input.GetAxis("Horizontal") * rotationSpeed;
           
        if (Input.GetKey(derrape) && Input.GetKey("a") || Input.GetKey(derrape) && Input.GetKey("d"))
        {

            this.GetComponent<Collider>().material.dynamicFriction = 0.4f;
        }
        else
        {
            this.GetComponent<Collider>().material.dynamicFriction = 1f;
        }

           translation *= Time.deltaTime;
           rotation *= Time.deltaTime;


        /*
        this.transform.position = new Vector3(
        Mathf.Lerp(this.transform.position.x, this.transform.position.x + this.transform.forward.x * translation, Time.deltaTime * 10),
        Mathf.Lerp(this.transform.position.y, this.transform.position.y, Time.deltaTime * 10),
        Mathf.Lerp(this.transform.position.z, this.transform.position.z + this.transform.forward.z * translation, Time.deltaTime * 10)
        );
        
        //print("vel " + this.GetComponent<Rigidbody>().velocity);
        */
        this.GetComponent<Rigidbody>().AddForce(this.transform.forward * translation);
        print(rotation);

            //transform.Translate(0, 0, translation);
            transform.Rotate(0, rotation, 0);
        
    }
}
