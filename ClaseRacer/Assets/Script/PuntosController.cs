﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PuntosController : MonoBehaviour
{

    public float puntos = 0;

    public int conosDerribados = 0;
    float v = 0;

    public Text txtPuntos;

    public KeyCode brake;

    // Start is called before the first frame update
    void Start()
    {
        txtPuntos.text = puntos.ToString();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        txtPuntos.text = puntos.ToString();

        v = this.gameObject.GetComponent<Rigidbody>().velocity.x * this.gameObject.transform.forward.x + this.gameObject.GetComponent<Rigidbody>().velocity.z * this.gameObject.transform.forward.z;
        PuntosDerrape();
    }

    private void PuntosDerrape()
    {
        if (v > 5 && Input.GetKey(brake))
        {
            puntos = puntos + 1f;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("cono") && collision.gameObject.GetComponent<ConosController>().tocado == false)
        {
            puntos = puntos - 50;
            conosDerribados++;
            if (puntos < 0)
            {
                puntos = 0;
            }

            collision.gameObject.GetComponent<ConosController>().tocado = true;
        }
    }
}
