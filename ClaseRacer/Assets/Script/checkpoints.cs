﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class checkpoints : MonoBehaviour
{

    int cont = 0;

    public GameObject puntosDrift;
    public GameObject pantallaFinish;

    public Text points;
    public Text conosDerribados;
    public Text topPoints;

    [SerializeField]
    private DriftBestPunctuation bestPunctuation;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag.Equals("checkpoint"))
        {
            if(cont < 3)
            {
                cont++;
            }
            print(cont);
        }
        if (collision.gameObject.tag.Equals("finish") && cont == 3)
        {
            if(this.gameObject.GetComponent<PuntosController>().puntos > bestPunctuation.BestPunctuation)
            {
                bestPunctuation.BestPunctuation = this.gameObject.GetComponent<PuntosController>().puntos;
            }

            this.gameObject.GetComponent<carController>().enabled = false;
            puntosDrift.gameObject.SetActive(false);
            pantallaFinish.gameObject.SetActive(true);
            points.text = this.gameObject.GetComponent<PuntosController>().puntos.ToString();
            conosDerribados.text = this.gameObject.GetComponent<PuntosController>().conosDerribados.ToString();
            topPoints.text = bestPunctuation.BestPunctuation.ToString();

        }

    }

}
